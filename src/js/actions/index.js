import { ADD_USER, DATA_REQUESTED } from "../constants/action_type"

export function addUser(payload) {
  return { type: ADD_USER, payload }
}

export function getData(url) {
  return { type: DATA_REQUESTED, payload: { url } }
}
