import { createStore, applyMiddleware, compose } from "redux"
import rootReducer from "../reducers/index"
import { forbiddenWordsMiddleware } from "../middleware"
import createSagaMiddleware from "redux-saga"
import thunk from "redux-thunk"
import apiSaga from "../sagas/api-saga"

const initialiseSagaMiddleware = createSagaMiddleware()

// line to use chrome extension
const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
  rootReducer,
  storeEnhancers(
    applyMiddleware(forbiddenWordsMiddleware, initialiseSagaMiddleware, thunk)
  )
)

initialiseSagaMiddleware.run(apiSaga)

export default store
