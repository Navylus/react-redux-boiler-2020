import { ADD_USER, FOUND_BAD_WORD, DATA_LOADED } from "../constants/action_type"

const initialState = {
  user: [],
  remoteArticles: []
}

function rootReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_USER:
      return Object.assign({}, state, {
        user: state.user.concat(action.payload),
      })
    case FOUND_BAD_WORD:
      console.log("bad word")
      break
    case DATA_LOADED:
      return Object.assign({}, state, {
        remoteArticles: state.remoteArticles.concat(action.payload),
      })
    default:
      break
  }
  return state
}

export default rootReducer
