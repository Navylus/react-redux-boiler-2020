import { ADD_USER, FOUND_BAD_WORD } from "../constants/action_type"

const forbiddenWords = ["test", "donald"]

export function forbiddenWordsMiddleware({ dispatch }) {
  return function (next) {
    return function (action) {
      // do your stuff
      if (action.type === ADD_USER) {
        const foundWord = forbiddenWords.filter((word) =>
          action.payload.name.includes(word)
        )

        if (foundWord.length) {
          return dispatch({ type: FOUND_BAD_WORD })
        }
      }
      return next(action)
    }
  }
}
