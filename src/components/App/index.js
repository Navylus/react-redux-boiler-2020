import React from "react"
import List from "./List"
import Post from "./Post"
import Form from "./Form"
import "./App.css"

function App() {
  return (
    <div className="App">
      <div>
        <h2>Users</h2>
        <List />
        <Form />
        <Post />
      </div>
    </div>
  )
}

export default App
