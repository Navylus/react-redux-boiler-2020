import React, { Component } from "react"
import { connect } from "react-redux"
import { addUser } from "../../js/actions"

function mapDispatchToProps(dispatch) {
  return {
    adduser: (user) => dispatch(addUser(user)),
  }
}

class ConnectedForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
    }
  }

  handleChange = (event) => {
    this.setState({ [event.target.id]: event.target.value })
  }

  handleSubmit = (event) => {
    event.preventDefault()
    const { name } = this.state
    this.props.adduser({ name })
    this.setState({ title: "" })
  }

  render() {
    const { name } = this.state
    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <label htmlFor="title">Name</label>
          <input
            type="text"
            id="name"
            value={name}
            onChange={this.handleChange}
          />
        </div>
        <button type="submit">SAVE</button>
      </form>
    )
  }
}

const Form = connect(null, mapDispatchToProps)(ConnectedForm)

export default Form
